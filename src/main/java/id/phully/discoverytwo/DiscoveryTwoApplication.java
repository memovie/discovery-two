package id.phully.discoverytwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class DiscoveryTwoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiscoveryTwoApplication.class, args);
    }

}

